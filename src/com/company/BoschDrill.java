package com.company;

public class BoschDrill implements IDrill {
    private String[] bitsTypes;
    private String[] workModes;
    private String producer = "Bosch";
    @Override public String getProducer()
    {
        return producer;
    }
    protected BoschDrill(String[] bitsTypes, String[] workModes) {
        this.bitsTypes = bitsTypes;
        this.workModes = workModes;
    }

    @Override
    public void drill() {

    }
}
