package com.company;

import com.company.DniproMCircularSaw;
import com.company.DniproMDrill;

public class DniproMFactory implements IAbstractFactory {

    @Override
    public IPliers createPliers() {
        String[] types = {"Big", "Small"};
        return new DniproMPliers(types);
    }

    @Override
    public IDrill createDrill() {
        String[] types = {"Big", "Shaped"};
        int[] size = {1, 3, 7};
        return new DniproMDrill(types, size);
    }

    @Override
    public IWeldingMachine createWeldingMachine() {
        String[] types = {"Big", "Ultra Light"};
        int[] size = {1, 2, 5};
        return new DniproMWeldingMachine(types, size);
    }

    @Override
    public ICircularSaw createCircularSaw() {
        String[] types = {"Big", "Shaped"};
        int[] size = {2, 4, 6};
        return new DniproMCircularSaw(types, size);
    }
}
