package com.company;

public interface IWeldingMachine
{
    void weld();
    String getProducer();
}
