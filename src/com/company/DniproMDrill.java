package com.company;

public class DniproMDrill implements IDrill {
    private int[] availableNozzlesSizes;
    private String[] bitsTypes;
    private String producer = "DniproM";
    @Override public String getProducer()
    {
        return producer;
    }
    protected DniproMDrill(String[] bitsTypes, int[] availableNozzlesSizes) {
        this.availableNozzlesSizes = availableNozzlesSizes;
        this.bitsTypes = bitsTypes;
    }

    @Override
    public void drill() {

    }
}
