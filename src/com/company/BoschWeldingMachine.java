package com.company;

public class BoschWeldingMachine implements IWeldingMachine {
    private String[] nozzlesTypes;
    private String[] workModes;
    private String producer = "Bosch";
    @Override public String getProducer()
    {
        return producer;
    }
    protected BoschWeldingMachine(String[] nozzlesTypes, String[] workModes) {
        this.nozzlesTypes = nozzlesTypes;
        this.workModes = workModes;
    }

    @Override
    public void weld() {

    }
}
