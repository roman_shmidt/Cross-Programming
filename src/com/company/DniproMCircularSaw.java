package com.company;

public class DniproMCircularSaw implements ICircularSaw {
    private int[] availableNozzlesSizes;
    private String[] nozzlesTypes;
    private String producer = "DniproM";
    @Override public String getProducer()
    {
        return producer;
    }
    protected DniproMCircularSaw(String[] nozzlesTypes, int[] availableNozzlesSizes) {
        this.availableNozzlesSizes = availableNozzlesSizes;
        this.nozzlesTypes = nozzlesTypes;
    }

    @Override
    public void saw() {

    }
}
