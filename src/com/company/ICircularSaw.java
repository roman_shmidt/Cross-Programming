package com.company;

public interface ICircularSaw
{
    void saw();
    String getProducer();
}
