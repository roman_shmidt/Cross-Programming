package com.company;

import com.company.BoschCircularSaw;
import com.company.BoschDrill;

public class BoschFactory implements IAbstractFactory {
    @Override
    public IPliers createPliers() {
        String[] types = {"Big", "Small"};
        return new BoschPliers(types);
    }

    @Override
    public IDrill createDrill() {
        String[] types = {"Big", "Shaped"};
        String[] modes = {"Fast", "LazyDrill"};
        return new BoschDrill(types, modes);
    }

    @Override
    public IWeldingMachine createWeldingMachine() {
        String[] types = {"Big", "Ultra Light"};
        String[] modes = {"Fast", "Easy weld"};
        return new BoschWeldingMachine(types, modes);
    }

    @Override
    public ICircularSaw createCircularSaw() {
        String[] types = {"Big", "Shaped"};
        String[] modes = {"Fast", "LazyDrill"};
        return new BoschCircularSaw(types, modes);
    }
}
