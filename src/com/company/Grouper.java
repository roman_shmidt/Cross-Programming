package com.company;

public class Grouper {
    private String toolName;
    private Integer toolFrequency;

    public Grouper(String toolName, Integer toolFrequency)
    {
        this.toolName = toolName;
        this.toolFrequency = toolFrequency;
    }

    public String getToolName() {
        return toolName;
    }

    public void setToolName(String name) {
        this.toolName = name;
    }

    public Integer getToolFrequency(){
        return toolFrequency;
    }

    public void setToolFrequency(Integer frequency){
        this.toolFrequency = frequency;
    }
}
