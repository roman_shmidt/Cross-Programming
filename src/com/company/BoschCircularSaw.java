package com.company;

public class BoschCircularSaw implements ICircularSaw {
    private String[] nozzlesTypes;
    private String[] workModes;
    private String producer = "Bosch";
    @Override public String getProducer()
    {
        return producer;
    }
    protected BoschCircularSaw(String[] nozzlesTypes, String[] workModes) {
        this.workModes = workModes;
        this.nozzlesTypes = nozzlesTypes;
    }

    @Override
    public void saw() {

    }
}
