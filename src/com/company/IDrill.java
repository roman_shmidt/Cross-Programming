package com.company;

public interface IDrill
{
    void drill();
    String getProducer();
}
