package com.company;

public interface IAbstractFactory
{
    IPliers createPliers();
    IDrill createDrill();
    IWeldingMachine createWeldingMachine();
    ICircularSaw createCircularSaw();
}
