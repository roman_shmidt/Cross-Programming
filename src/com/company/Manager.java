package com.company;

import java.util.*;

public class Manager {

    public ArrayList<Grouper> go = new ArrayList<>();

    private static class AppearFrequency implements Comparator<Grouper>{
        @Override
        public int compare(Grouper o1, Grouper o2) {
            return o1.getToolFrequency() > o2.getToolFrequency() ? 1 : o1.getToolFrequency() < o2.getToolFrequency() ? -1 : 0;
        }
    }

    private class AppearFrequency_inner implements Comparator<Grouper>
    {

        @Override
        public int compare(Grouper o1, Grouper o2) {
            return o1.getToolFrequency() > o2.getToolFrequency() ? 1 : o1.getToolFrequency() < o2.getToolFrequency() ? -1 : 0;
        }
    }

    Hashtable<Integer, Object[]> objectMade = new Hashtable<>();
    public void initializeObjects()
    {

        BoschFactory boschFactory = new BoschFactory();
        DniproMFactory dniproMFactory = new DniproMFactory();

        ICircularSaw  boschCircularSaw = boschFactory.createCircularSaw();
        IDrill boschDrill = boschFactory.createDrill();
        IPliers boschPliers = boschFactory.createPliers();
        IWeldingMachine boschWeldingMachine = boschFactory.createWeldingMachine();


        ICircularSaw  dniproMCircularSaw = dniproMFactory.createCircularSaw();
        IDrill dniproMDrill = dniproMFactory.createDrill();
        IPliers dniproMPliers = dniproMFactory.createPliers();
        IWeldingMachine dniproMWeldingMachine = dniproMFactory.createWeldingMachine();

        Object[] chair = {boschCircularSaw, dniproMDrill, boschWeldingMachine};
        Object[] sofa = {boschCircularSaw, dniproMPliers, dniproMDrill, boschWeldingMachine};
        Object[] wardrobe = {boschDrill, boschPliers, dniproMWeldingMachine, boschCircularSaw};
        Object[] bed = {dniproMCircularSaw, dniproMPliers, boschDrill};
        Object[] parquet = {boschCircularSaw, boschWeldingMachine, dniproMPliers};
        Object[] door = {boschCircularSaw, boschPliers, boschWeldingMachine, boschDrill};
        objectMade.put(1, chair);
        objectMade.put(2, sofa);
        objectMade.put(3, wardrobe);
        objectMade.put(4, bed);
        objectMade.put(5, parquet);
        objectMade.put(6, door);
    }

    public void search()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.print("""
                Choose object to find which tools you need for it:
                1. Chair
                2. Sofa
                3. Wardrobe
                4. Bed
                5. Parquet
                6. Door
                It will be sorted by producer of tools.
                """);
        int choose = scanner.nextInt();
        ArrayList<String> boschList = new ArrayList<>();
        ArrayList<String> dniproMList = new ArrayList<>();

        for(int i = 0; i < objectMade.get(choose).length; i++)
        {
            if(objectMade.get(choose)[i].getClass() == BoschPliers.class ||  objectMade.get(choose)[i].getClass() == DniproMPliers.class)
            {
                if(((IPliers)objectMade.get(choose)[i]).getProducer() == "DniproM")
                {
                    dniproMList.add(DniproMPliers.class.getSimpleName());
                }
                else
                {
                    boschList.add(BoschPliers.class.getSimpleName());
                }
                //object.put(((IPliers)objectMade.get(choose)[i]).getProducer(), IPliers.class.getName());
            }
            else if(objectMade.get(choose)[i].getClass() == BoschDrill.class || objectMade.get(choose)[i].getClass() == DniproMDrill.class)
            {
                if(((IDrill)objectMade.get(choose)[i]).getProducer() == "DniproM")
                {
                    dniproMList.add(DniproMDrill.class.getSimpleName());
                }
                else
                {
                    boschList.add(BoschDrill.class.getSimpleName());
                }
                //object.put(((IDrill)objectMade.get(choose)[i]).getProducer(), IDrill.class.getName());
            }
            else if(objectMade.get(choose)[i].getClass() == BoschCircularSaw.class || objectMade.get(choose)[i].getClass() == DniproMCircularSaw.class)
            {
                if(((ICircularSaw)objectMade.get(choose)[i]).getProducer() == "DniproM")
                {
                    dniproMList.add(DniproMCircularSaw.class.getSimpleName());
                }
                else
                {
                    boschList.add(BoschCircularSaw.class.getSimpleName());
                }
                //object.put(((ICircularSaw)objectMade.get(choose)[i]).getProducer(), ICircularSaw.class.getName());
            }
            else if(objectMade.get(choose)[i].getClass() == BoschWeldingMachine.class || objectMade.get(choose)[i].getClass() == DniproMWeldingMachine.class)
            {
                if(((IWeldingMachine)objectMade.get(choose)[i]).getProducer() == "DniproM")
                {
                    dniproMList.add(DniproMWeldingMachine.class.getSimpleName());
                }
                else
                {
                    boschList.add(BoschWeldingMachine.class.getSimpleName());
                }
            }
        }
        for(var yes: boschList)
        {
            System.out.println("Bosch: " + yes);
        }
        for(var yes: dniproMList)
        {
            System.out.println("DniproM: " + yes);
        }

        HashMap<String, Integer> hash = new HashMap<>();
        for(int i = 1; i <= objectMade.size(); i++)
        {
            for(int j = 0; j < objectMade.get(i).length; j++)
            {
                if(objectMade.get(i)[j].getClass() == BoschPliers.class ||  objectMade.get(i)[j].getClass() == DniproMPliers.class)
                {
                    if(((IPliers)objectMade.get(i)[j]).getProducer() == "DniproM")
                    {
                        if(!hash.containsKey(DniproMPliers.class.getSimpleName()))
                        {
                            hash.put(DniproMPliers.class.getSimpleName(), 1);
                        }
                        else
                        {
                            hash.put(DniproMPliers.class.getSimpleName(), hash.get(DniproMPliers.class.getSimpleName()) + 1);
                        }
                    }
                    else
                    {
                        if(!hash.containsKey(BoschPliers.class.getSimpleName()))
                        {
                            hash.put(BoschPliers.class.getSimpleName(), 1);
                        }
                        else
                        {
                            hash.put(BoschPliers.class.getSimpleName(), hash.get(BoschPliers.class.getSimpleName()) + 1);
                        }
                    }
                }
                else if(objectMade.get(i)[j].getClass() == BoschDrill.class || objectMade.get(i)[j].getClass() == DniproMDrill.class)
                {
                    if(((IDrill)objectMade.get(i)[j]).getProducer() == "DniproM")
                    {
                        if(!hash.containsKey(DniproMDrill.class.getSimpleName()))
                        {
                            hash.put(DniproMDrill.class.getSimpleName(), 1);
                        }
                        else
                        {
                            hash.put(DniproMDrill.class.getSimpleName(), hash.get(DniproMDrill.class.getSimpleName()) + 1);
                        }
                    }
                    else
                    {
                        if(!hash.containsKey(BoschDrill.class.getSimpleName()))
                        {
                            hash.put(BoschDrill.class.getSimpleName(), 1);
                        }
                        else
                        {
                            hash.put(BoschDrill.class.getSimpleName(), hash.get(BoschDrill.class.getSimpleName()) + 1);
                        }
                    }
                }
                else if(objectMade.get(i)[j].getClass() == BoschCircularSaw.class || objectMade.get(i)[j].getClass() == DniproMCircularSaw.class)
                {
                    if(((ICircularSaw)objectMade.get(i)[j]).getProducer() == "DniproM")
                    {
                        if(!hash.containsKey(DniproMCircularSaw.class.getSimpleName()))
                        {
                            hash.put(DniproMCircularSaw.class.getSimpleName(), 1);
                        }
                        else
                        {
                            hash.put(DniproMCircularSaw.class.getSimpleName(), hash.get(DniproMCircularSaw.class.getSimpleName()) + 1);
                        }
                    }
                    else
                    {
                        if(!hash.containsKey(BoschCircularSaw.class.getSimpleName()))
                        {
                            hash.put(BoschCircularSaw.class.getSimpleName(), 1);
                        }
                        else
                        {
                            hash.put(BoschCircularSaw.class.getSimpleName(), hash.get(BoschCircularSaw.class.getSimpleName()) + 1);
                        }
                    }
                }
                else if(objectMade.get(i)[j].getClass() == BoschWeldingMachine.class || objectMade.get(i)[j].getClass() == DniproMWeldingMachine.class)
                {
                    if(((IWeldingMachine)objectMade.get(i)[j]).getProducer() == "DniproM")
                    {
                        if(!hash.containsKey(DniproMWeldingMachine.class.getSimpleName()))
                        {
                            hash.put(DniproMWeldingMachine.class.getSimpleName(), 1);
                        }
                        else
                        {
                            hash.put(DniproMWeldingMachine.class.getSimpleName(), hash.get(DniproMWeldingMachine.class.getSimpleName()) + 1);
                        }
                    }
                    else
                    {
                        if(!hash.containsKey(BoschWeldingMachine.class.getSimpleName()))
                        {
                            hash.put(BoschWeldingMachine.class.getSimpleName(), 1);
                        }
                        else
                        {
                            hash.put(BoschWeldingMachine.class.getSimpleName(), hash.get(BoschWeldingMachine.class.getSimpleName()) + 1);
                        }
                    }
                }
            }

        }
        System.out.print("Choose 1 if up, 2 if down: ");
        int choose2 = scanner.nextInt();

        String[] toolsNames = new String[hash.keySet().size()];
        hash.keySet().toArray(toolsNames);

        for (int i = 0; i < hash.size(); i++)
        {
            Grouper gogo = new Grouper(toolsNames[i], hash.get(toolsNames[i]));
            go.add(gogo);
        }
        System.out.print("Choose 1 if Static Inner, 2 if Inner, 3 if Anonymous, 4 if Lambda: ");
        int choose3 = scanner.nextInt();
        switch (choose3) {
            case 1:
            go.sort(new Manager.AppearFrequency());
                for (var elem: go)
                {
                    System.out.println(elem.getToolName() + " " + elem.getToolFrequency());
                }
            break;
            case 2:
            go.sort(this.new AppearFrequency_inner());
                for (var elem: go)
                {
                    System.out.println(elem.getToolName() + " " + elem.getToolFrequency());
                }
            break;
            case 3:
            sortByAppearFrequencyAnonymous();
            break;
            case 4:
                if(choose2 == 1) {
                    sortByAppearFrequencyLambda(true);
                }
                if(choose2 == 2) {
                    sortByAppearFrequencyLambda(false);
                }
                for (var elem: go)
                {
                    System.out.println(elem.getToolName() + " " + elem.getToolFrequency());
                }
            break;
        }
    }

    private void sortByAppearFrequencyAnonymous()
    {
        go.sort(new Comparator<Grouper>() {
            @Override
            public int compare(Grouper o1, Grouper o2) {
                return o1.getToolFrequency() > o2.getToolFrequency() ? 1 :
                        o1.getToolFrequency() < o2.getToolFrequency() ? -1 : 0;
            }
        });
    }
    private  void sortByAppearFrequencyLambda(boolean ascending)
    {
        //apartments.sort(Comparator.naturalOrder()); - works but with that field that is used in Comparable interface in Flat class - how change field?

        go.sort((o1,o2) -> o1.getToolFrequency().compareTo(o2.getToolFrequency()));//  - works (lambda)
        //apartments.sort(Comparator.comparing(Flat::getResidentalArea));// - works
        if(!ascending)
        {
            Collections.reverse(go);
        }

    }
}
