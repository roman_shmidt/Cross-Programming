package com.company;

public class DniproMWeldingMachine implements IWeldingMachine {
    private int[] availableNozzlesSizes;
    private String[] nozzlesTypes;
    private String producer = "DniproM";
    @Override
    public String getProducer()
    {
        return producer;
    }
    public DniproMWeldingMachine(String[] nozzlesTypes, int[] availableNozzlesSizes) {
        this.availableNozzlesSizes = availableNozzlesSizes;
        this.nozzlesTypes = nozzlesTypes;
    }

    @Override
    public void weld() {

    }
}
